# This was run on a CSV export from google spreadsheet of existing river data
# note that quotation marks were stripped out in order to split "lat, long" single cat into multiple cats
# assumes idents are in progressive order
# still thought it would be useful to have kicking around the repo
#
# Author: zminster
# Date: August 24, 2016

import sqlite3
import csv

conn = sqlite3.connect('rivers.db.sqlite')
c = conn.cursor()


with open('cleaner_river.csv', 'rb') as csvfile:
	csvreader = csv.reader(csvfile, delimiter=',', quotechar='\"')
	ident = 1
	for row in csvreader:
		for i in range(len(row)):
			if (row[i] == ""):
				row[i] = "NULL"
			if (" " in row[i] or "http" in row[i]):
				row[i] = "\"" + row[i] + "\""
		print("UPDATE stats SET site_number = " + row[0] + ", lat = " + row[1] + ", lon = " + row[2] + ", grid_number = " + row[3] + ", benthic_score = " + row[4] + ", flow_rate = " + row[5] + ", phosphates = " + row[6] + ", temperature = " + row[7] + ", ph = " + row[8] + ", conductivity = " + row[9] + ", ammonium = " + row[10] + ", nitrates = " + row[11] + ", ecoli = " + row[12] + ", turbidity = " + row[13] + ", do_percent = " + row[14] + ", bod_percent = " + row[15] + ", soil = " + row[16] + ", plankton = " + row[17] + ", fish = " + row[18] + ", bod_column = " + row[19] + ", bod_hr = " + row[20] + ", v_constricta = " + row[21] + ", s_undulatus = " + row[22] + ", p_collina = " + row[23] + " WHERE ident=" + str(ident))
		conn.execute("UPDATE stats SET site_number = " + row[0] + ", lat = " + row[1] + ", lon = " + row[2] + ", grid_number = " + row[3] + ", benthic_score = " + row[4] + ", flow_rate = " + row[5] + ", phosphates = " + row[6] + ", temperature = " + row[7] + ", ph = " + row[8] + ", conductivity = " + row[9] + ", ammonium = " + row[10] + ", nitrates = " + row[11] + ", ecoli = " + row[12] + ", turbidity = " + row[13] + ", do_percent = " + row[14] + ", bod_percent = " + row[15] + ", soil = " + row[16] + ", plankton = " + row[17] + ", fish = " + row[18] + ", bod_column = " + row[19] + ", bod_hr = " + row[20] + ", v_constricta = " + row[21] + ", s_undulatus = " + row[22] + ", p_collina = " + row[23] + " WHERE ident=" + str(ident))
		ident += 1

conn.commit()

conn.close()
